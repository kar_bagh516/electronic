import Vue from 'vue'
import 'es6-promise/auto'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import auth from "./auth"
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import CountryFlag from 'vue-country-flag'

import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import CKEditor from '@ckeditor/ckeditor5-vue'

library.add(fas,far,fab)

Vue.mixin(require('./trans'))
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('country-flag',CountryFlag)

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(CKEditor)

import App from './views/App'
import Welcome from './views/Pages/Welcome'
import Collections from './views/Pages/Collections'
import Collection from './views/Pages/Collection'
import Item from './views/Pages/Item'
import Services from './views/Pages/Services'
import NewOrder from "./views/Pages/NewOrder";
import DynamicPage from "./views/Pages/DynamicPage";

// Admin Page
import Admin from "./views/Admin";
import Home from "./views/Admin/Home";


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Welcome
        }, {
            path: '/collections',
            name: 'collections',
            component: Collections
        }, {
            path: '/collection/:id',
            name: 'collection',
            component: Collection
        }, {
            path: '/collection/:collection_id/item/:id',
            name: 'item',
            component: Item
        }, {
            path: '/new-order/:device',
            name: 'newOrder',
            component: NewOrder
        }, {
            name: 'services',
            path: '/services',
            component: Services,
        },{
            name: 'page',
            path: '/page/:page',
            component: DynamicPage,
        }, {
            name: 'admin',
            path: '/admin',
            meta: {admin: true},
            component: Admin,
            children:[
                {
                    name: 'admin.dashboard',
                    meta: {admin: true, middleware: [auth] },
                    path: '/admin/dashboard',
                    component: Home,
                },
            ]
         },
    ],
});

function nextFactory(context, middleware, index) {
      const subsequentMiddleware = middleware[index];
      // If no subsequent Middleware exists,
          // the default `next()` callback is returned.
              if (!subsequentMiddleware) return context.next;

          return (...parameters) => {
            // Run the default Vue Router `next()` callback first.
                context.next(...parameters);
            // Then run the subsequent Middleware with a new
                // `nextMiddleware()` callback.
                    const nextMiddleware = nextFactory(context, middleware, index + 1);
            subsequentMiddleware({ ...context, next: nextMiddleware });
          };
    }

router.beforeEach((to, from, next) => {
      if (to.meta.middleware) {
            const middleware = Array.isArray(to.meta.middleware)
              ? to.meta.middleware
                  : [to.meta.middleware];

                const context = {
                  from,
                  next,
                  router,
                  to,
                };
            const nextMiddleware = nextFactory(context, middleware, 1);

                return middleware[0]({ ...context, next: nextMiddleware });
          }

          return next();
    });


export default router;

const app = new Vue({
    el: '#app',
    components: {App},
    router,
});



