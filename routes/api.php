<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Home Page Lists
Route::post('/featured', 'SinglePageController@featured')->name('featured');

//List Collections
Route::post('/device', 'DeviceController@show')->name('device');
Route::post('/rateDevice', 'DeviceController@rate')->name('rateDevice');
Route::post('/deviceList', 'DeviceController@index')->name('deviceList');
Route::post('/deviceListAll', 'DeviceController@list')->name('deviceListAll');
Route::post('/collectionList', 'CollectionController@index')->name('collectionList');
Route::post('/serviceList', 'ServiceController@index')->name('serviceList');
Route::post('/serviceListAll', 'ServiceController@list')->name('serviceListAll');
Route::post('/discountList', 'ServiceController@dicounts')->name('discount');
Route::post('/newCollectionList', 'ServiceController@news')->name('newCollectionList');
Route::post('/bannerList', 'BannerController@list')->name('bannerList');

//Add Data
Route::post('/addCollection', 'CollectionController@create')->name('addCollection');
Route::post('/removeCollection', 'CollectionController@destroy')->name('removeCollection');
Route::post('/addService', 'ServiceController@create')->name('addService');
Route::post('/removeDevice', 'DeviceController@destroy')->name('removeDevice');
Route::post('/addDevice', 'DeviceController@create')->name('addDevice');
Route::post('/addBanner', 'BannerController@create')->name('addBanner');
Route::post('/removeBanner', 'BannerController@destroy')->name('removeBanner');
Route::post('/offBanner', 'BannerController@off')->name('offBanner');
Route::post('/removeService', 'ServiceController@destroy')->name('removeService');
Route::post('/offService', 'ServiceController@off')->name('offService');

//Admin Panel EndPoints
Route::post('/login', 'Auth\LoginController@login')->name('dashboard');
Route::post('/authentication', 'Auth\LoginController@authentication')->name('authentication');
Route::post('/dashboardData', 'SinglePageController@dashboard')->name('dashboard');


Route::post('/image/store', 'ImageUploadController@uploadImage')->name('uploadImage');
Route::post('/images/store', 'ImageUploadController@uploadImages')->name('uploadImages');

Route::post('/order', 'OrderController@create')->name('order');
Route::post('/orderListAll', 'OrderController@index')->name('orderListAll');


Route::prefix('/page')->group(function (){
    Route::post('/add', 'SinglePageController@addDinamicPage')->name('add');
    Route::post('/get', 'SinglePageController@showDinamicPage')->name('get');
    Route::post('/menu/list', 'SinglePageController@showDynamicList')->name('menuList');
    Route::post('/remove', 'SinglePageController@removeDinamicPage')->name('remove');
    Route::post('/edit', 'SinglePageController@editDinamicPage')->name('edit');
    Route::post('/get/all', 'SinglePageController@getAllDinamicPage')->name('getAll');
});

Route::post('/save/socials', 'SinglePageController@saveSocials')->name('saveSocials');

