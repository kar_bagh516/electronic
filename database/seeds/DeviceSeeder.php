<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = [
            "https://cdn.shopify.com/s/files/1/0543/1637/products/1347910998.jpg?v=1484754685",
            "https://cdn.shopify.com/s/files/1/0543/1637/products/1347911009.jpg?v=1484754685",
            "https://cdn.shopify.com/s/files/1/0543/1637/products/1347911020.jpg?v=1484754685",
            "https://cdn.shopify.com/s/files/1/0543/1637/products/1347911040.jpg?v=1484754685",
        ];

        for ($j = 0;$j <= 11;$j++){
            $name = Str::random(10);
            $model = Str::random(20);

            DB::table('devices')->insert([
                'collection_id' => rand(1,10),
                'brand' => $name,
                'model' => $model,
                'description_am' => $model,
                'description_ru' => $model,
                'description_en' => $model,
                'images' => json_encode($images),
                'made' => rand(2010,2030),
                'new_price' => rand(1200,3000),
                'old_price' => $j%2 === 1 ? 0 : rand(3000,5000),
                'used' => $j%2 === 0,
                'in_stock' => rand(0,10),
                'awesome' => rand(1,100),
                'great' => rand(1,100),
                'very_good' => rand(1,100),
                'good' => rand(1,100),
                'bad' => rand(1,100),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
