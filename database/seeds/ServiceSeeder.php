<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i <= 11; $i++) {
            $name = Str::random(20);
            DB::table('services')->insert([
                'name' => $name,
                'description_en' => $name,
                'description_ru' => $name,
                'description_am' => $name,
                'active' => $i%2 === 1 ? 1 : 0,
                'image' => "https://cdn.shopify.com/s/files/1/0543/1637/products/1347910998.jpg?v=1484754685",
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
