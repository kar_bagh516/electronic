<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ParameterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0;$i <= 11;$i++){
            $name = Str::random(20);

            DB::table('parameters')->insert([
                'device_id' => rand(1,10),
                'parameter_en' => $name,
                'parameter_ru' => $name,
                'parameter_am' => $name,
                'value' => $name,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
