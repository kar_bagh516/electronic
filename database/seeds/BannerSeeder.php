<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $images = [
            "https://cdn.shopify.com/s/files/1/0543/1637/products/1347910998.jpg?v=1484754685",
            "https://cdn.shopify.com/s/files/1/0543/1637/products/1347911009.jpg?v=1484754685",
            "https://cdn.shopify.com/s/files/1/0543/1637/products/1347911020.jpg?v=1484754685",
            "https://cdn.shopify.com/s/files/1/0543/1637/products/1347911040.jpg?v=1484754685",
        ];

        for ($i = 0;$i <= 3;$i++){
            $name = Str::random(10);
            DB::table('banners')->insert([
                'title' => $i%2 === 1 ? $name : '',
                'image' => $images[$i],
                'active' => $i%2 === 1 ? 1 : 0,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
