<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class CollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0;$i <= 11;$i++){
            $name_en = Str::random(10);
            DB::table('collections')->insert([
                'name_am' => 'Ապրանք',
                'name_ru' => 'Продукт',
                'name_en' => $name_en,
                'path' => str_replace(' ','.', $name_en),
                'image' => "https://cdn.shopify.com/s/files/1/0543/1637/collections/camera_125x.jpg?v=1480360378",
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
