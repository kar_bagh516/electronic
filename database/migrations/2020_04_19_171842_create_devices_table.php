<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('collection_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->string('brand');
            $table->string('model');
            $table->string('description_am')->nullable();
            $table->string('description_ru')->nullable();
            $table->string('description_en')->nullable();
            $table->json('images');
            $table->integer('new_price');
            $table->integer('old_price');
            $table->year('made')->nullable();
            $table->integer('used');
            $table->integer('in_stock');
            $table->text('deposit_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
