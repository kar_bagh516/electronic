<?php

namespace App\Http\Controllers;

use App\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class CollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json([
            'collections' => Collection::select(DB::raw('count(devices.id) as count') , "collections.*")->groupBy('collections.id')->leftJoin('devices' , 'devices.collection_id' , '=' , 'collections.id')->get(),
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $message = $this->validate($request, [
            'name_am' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
            'image' => 'required',
        ]);


        $create = Collection::create([
            'name_am' => $request->name_am,
            'name_ru' => $request->name_ru,
            'name_en' => $request->name_en,
            'image' => $request->image,
        ])->all();

        return response()->json([
            'collections' => $create,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function show(Collection $collection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collection $collection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        Collection::find($request->id)->delete();

        return response()->json([
            'message' => [
                'title' => 'Կատեգորիան հեռացված է։',
                'body' => 'Կատեգորիան, և այդ կատեգորիան ունեցող ապրանքները հեռացված են։',
            ], Response::HTTP_OK
        ]);
    }
}
