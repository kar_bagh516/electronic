<?php

namespace App\Http\Controllers;

use App\Device;
use App\Parameter;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private const ORDER = [
        [
            'column' => 'brand',
            'order' => 'asc'
        ], [
            'column' => 'brand',
            'order' => 'desc'
        ], [
            'column' => 'new_price',
            'order' => 'asc'
        ], [
            'column' => 'new_price',
            'order' => 'desc'
        ], [
            'column' => 'made',
            'order' => 'asc'
        ], [
            'column' => 'made',
            'order' => 'desc'
        ],
    ];


    public function index(Request $request)
    {
        return response()->json([
            'devices' => Device::where('collection_id', '=', $request['id'])->orderBy(self::ORDER[$request->sort]['column'], self::ORDER[$request->sort]['order'])->select('devices.*', 'collections.path', 'collections.name_am', 'collections.name_ru', 'collections.name_en')->join('collections', 'devices.collection_id', '=', 'collections.id')->get(),
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $type = $request['type'];

        return response()->json([
            'devices' => Device::join('collections', 'devices.collection_id', '=', 'collections.id')->select('devices.*', 'collections.path', 'collections.name_am', 'collections.name_ru', 'collections.name_en')->get(),
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {

        $this->validate($request, [
            'brand' => 'required',
            'model' => 'required',
            'category' => 'required',
            'in_stock' => 'required',
            'new_price' => 'required',
            'old_price' => 'required',
            'made_year' => 'required',
            'images' => 'required',
            'used' => 'required',
            'description_am' => 'required',
            'description_ru' => 'required',
            'description_en' => 'required',
        ]);

        $create = Device::create([
            'brand' => $request->brand,
            'model' => $request->model,
            'collection_id' => $request->category,
            'in_stock' => $request->in_stock,
            'new_price' => $request->new_price,
            'old_price' => $request->old_price,
            'made' => $request->made_year,
            'used' => $request->used,
            'images' => json_encode($request->images),
            'description_am' => $request->description_am,
            'description_ru' => $request->description_ru,
            'description_en' => $request->description_en,
            'deposit_details' => $request->deposit_am,
        ]);

        foreach ($request->parameters as $parameter) {
            Parameter::create([
                'device_id' => $create->id,
                'parameter_en' => $parameter['en'],
                'parameter_ru' => $parameter['ru'],
                'parameter_am' => $parameter['am'],
                'value' => $parameter['value'],
            ]);
        }

        return response()->json([
           'device' => $create,
           'message' => "Device has be created"
        ] ,Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Device $device
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {


        return response()->json([
            'device' => Device::detailed($request->id) ,
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    function rate(Request $request)
    {
        $device = Device::where('id', $request['id']);
        $device->update(['rating' => $request['value']]);
        $device->increment('rated');

        return response()->json([
            'result' => $device->first()
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Device $device
     * @return \Illuminate\Http\Response
     */
    public function edit(Device $device)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Device $device
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Device $device)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Device $device
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        Device::find($request->id)->delete();

        return response()->json([
            'message' => [
                'title' => 'Ապրանքները հեռացված է։',
                'body' => 'Ապրանքները հեռացված են և այլևս չեն ցուցադրվի։',
            ], Response::HTTP_OK
        ]);
    }
}
