<?php

namespace App\Http\Controllers;

use App\Device;
use App\Order;
use Symfony\Component\HttpFoundation\Response;
use Validator;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderList = Order::leftJoin('devices' , 'devices.id' , 'orders.device_id')
            ->select('orders.*', 'orders.name as userName' , 'devices.brand','devices.model','devices.new_price')
            ->get();

        return \response()->json([
            'orderList' => $orderList
        ] , Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
         $inputs = [
            "name" => "required",
            "surname" => "required",
            "address" => "required",
            "code" => "required",
            "mobile" => "required",
            "email" => "required|email"
        ];

         $messages = [
            "name.required" => "Նշեք ձեր անունը",
            "surname.required" => "Նշեք ձեր ազգանունը",
            "address.required" => "Նշեք ձեր հասցեն",
            "code.required" => "Նշեք ձեր երկրի կոդը",
            "mobile.required" => "Նշեք ձեր հեռախոսահամարը",
            "email.required" => "Նշեք ձեր էլ․փոստի հասցեն",
            "email.email" => "Էլ․ փոստը սխալէ նշված"
        ];

        $validation = Validator::make($request->all() , $inputs , $messages);

        if ($validation->fails()){
            return \response()->json(
                [
                     'messages'=> $validation->messages()
                ], Response::HTTP_BAD_REQUEST

            );
        }

        Order::create([
            "name" => $request->name,
            "messengers" => json_encode($request->messangers),
            "surname" => $request->surname,
            "device_id" => $request->device_id,
            "address" => $request->address,
            "mobile" => $request->code.$request->mobile,
            "email" => $request->email,
            "status" => 0,
            "pay_method" => 0,
        ]);

        $device = Device::find($request->device_id)->decrement('in_stock');

        return response()->json(
            ['message' => [
                'title' => 'Պատվերն ընդունված է',
                'body' => 'Շնորհավուրում ենք դուք հաջողությամբ կատարեցիք պատվերը, այն կառաքվի նշված հասցեյով, խնդրում ենք հետևել հեռախոսազանգերին կամ էլ․փոստին, շնորհակալ ենք պատվերի համար։']
            ], Response::HTTP_OK
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
