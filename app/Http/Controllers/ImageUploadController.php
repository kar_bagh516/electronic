<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;

class ImageUploadController extends Controller
{

    private function store($data){


        if (!file_exists($data->path)) {
            File::makeDirectory($data->path, 0777, true, true);
        }

        $name = str_replace(' ' , '-',$data->name );
        $today = Carbon::now()->format('d-m-y');
        $format = $data->image->getClientOriginalExtension();
        $name = $name."-".$today.".".$format;

        Image::make($data->image)
            ->resize(300, 200)
            ->save("$data->path/$name");

        return $name;
    }

    public function uploadImage(Request $request)
    {
        // Validate (size is in KB)
        $request->validate([
            'image' => 'required|mimes:jpeg,jpg,png|file|image|max:1024',
        ]);

        $name = $this->store($request);
        return response()->json(['image' => $name] , Response::HTTP_OK);

    }
}
