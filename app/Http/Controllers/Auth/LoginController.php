<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\UserAuth;
use App\Providers\RouteServiceProvider;
use App\User;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

        function login(Request $request){
            $user = User::where(['username' => $request['username']])->first();

            if ($user){
                $verification = password_verify($request['password'] , $user->password);
                if ($verification){
                    Mail::to($user->email)->send(new UserAuth(['token' => $user->token , 'name' => $user->name , 'surname' => $user->surname]));
                    $data = ['auth' => true , 'message' => 'We have send you email a verification token, please Enter it here.'];
                }else{
                    $data = ['auth' => false , 'message' => "Passwords is not correct"];
                }
            }else{
                $data = ['auth' => false , 'message' => 'User is not defined. Please write correct username'];
            }

            return response()->json($data , Response::HTTP_OK);
        }

        function authentication(Request $request){
            $user = User::where(['token' => $request['token']])->first();

            if ($user){
                $data = ['auth' => true , 'user' => $user, 'message' => 'Welcome to Dashboard'];
            }else{
                $data = ['auth' => false , 'message' => 'Token is not correct'];
            }

            return response()->json($data , Response::HTTP_OK);

        }
}
