<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return  response()->json([
            "services" => Service::where('active' , 1)->get(),
        ] , Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        return  response()->json([
            "services" => Service::all(),
        ] , Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description_am' => 'required',
            'description_ru' => 'required',
            'description_en' => 'required',
        ]);


        $create = Service::create([
            'name' => $request->name,
            'description_am' => $request->description_am,
            'description_ru' => $request->description_ru,
            'description_en' => $request->description_en,
            'image' => $request->image,
        ])->all();

        return response()->json([
            'services' => $create,
        ],Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        Service::find($request->id)->delete();

        return \response()->json([
            'message' => [
                'title' => 'Ծառայությունը հեռացված է',
                'body' => 'Ծառայությունը հեռացված է, և այլևս չի ցուցադրվելու։'
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Banner $banner
     * @return \Illuminate\Http\JsonResponse
     */
    public function off(Request $requst)
    {
        $baner = Service::find($requst->id);
        if ($requst->status){
            $baner->decrement('active');
            $message = [
                'title' => 'Ծառայությունը անջատված է։',
                'body' => 'Ծառայությունը անջատված է,և չի ցուցադրվելու մինչ նորից չմիացնեք։'
            ];
        }else{
            $baner->increment('active');
            $message = [
                'title' => 'Ծառայությունը միացված է։',
                'body' => 'Ծառայությունը միացված է,և ցուցադրվելու է մինչ նորից ակտիվանալը։'
            ];
        }

        return \response()->json([
            'message' => $message
        ]);
    }
}
