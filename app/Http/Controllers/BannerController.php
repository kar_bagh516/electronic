<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        return response()->json([
            'banners' => Banner::all()
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $message = $this->validate($request, [
            'title' => 'required',
            'image' => 'required',
            'status' => 'required',
        ]);


        $create = Banner::create([
            'title' => $request->title,
            'image' => $request->image,
            'active' => $request->status,
        ])->all();

        return response()->json([
            'banners' => $create,
        ], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Banner $banner
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $requst)
    {
        Banner::find($requst->id)->delete();

        return \response()->json([
            'message' => [
                'title' => 'Գովազդը հեռացված է։',
                'body' => 'Գովազդը հեռացված է,և այլևս չի ցուցադրվելու։'
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Banner $banner
     * @return \Illuminate\Http\JsonResponse
     */
    public function off(Request $requst)
    {
        $baner = Banner::find($requst->id);
        if ($requst->status){
            $baner->decrement('active');
            $message = [
                'title' => 'Գովազդը անջատված է։',
                'body' => 'Գովազդը անջատված է,և չի ցուցադրվելու մինչ նորից չմիացնեք։'
            ];
        }else{
            $baner->increment('active');
            $message = [
                'title' => 'Գովազդը միացված է։',
                'body' => 'Գովազդը միացված է,և ցուցադրվելու է մինչ նորից ակտիվանալը։'
            ];
        }

        return \response()->json([
            'message' => $message
        ]);
    }
}
