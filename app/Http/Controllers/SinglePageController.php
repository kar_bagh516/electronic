<?php

namespace App\Http\Controllers;

use App\Service;
use Validator;
use Illuminate\Http\Request;
use App\Collection;
use App\Device;
use App\Order;
use App\Banner;
use App\Page;

use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class SinglePageController extends Controller
{

    public function index()
    {
        return view("welcome");
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function featured(){
        $collections = Collection::all()->take(5);
        $featured = Device::featured()->take(5)->get();
        $news = Device::news()->take(4)->get();
        $discounts = Device::discount()->take(4)->get();
        $slider = Banner::where('active' , 1)->get();

        return response()->json([
            'collections' => $collections,
            'featured' => $featured,
            'news' => $news,
            'discounts' => $discounts,
            'slider' => $slider,
        ], Response::HTTP_OK);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function dashboard(){
        $collections = Collection::count();
        $collections_count_mounts = Collection::select(
            'created_at',
            DB::raw('count(*) as count'),
            DB::raw("DATE_FORMAT(created_at, '%m-%Y') date"),
            DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
            ->groupBy('year','month')
            ->get();
        $devices = Device::count();
        $devices_count_mounts = Device::devices_count_mounts()->get();
        $orders = Order::count();
        $orders_count_mounts = Order::select(
            'created_at',
            DB::raw('count(*) as count'),
            DB::raw("DATE_FORMAT(created_at, '%m-%Y') date"),
            DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
            ->groupBy('year','month')
            ->get();
        $services = Service::count();
        $services_count_mounts = Service::select(
            'created_at',
            DB::raw('count(*) as count'),
            DB::raw("DATE_FORMAT(created_at, '%m-%Y') date"),
            DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
            ->groupBy('year','month')
            ->get();
        $banner = Banner::count();
        $banners_count_mounts = Banner::select(
            'created_at',
            DB::raw('count(*) as count'),
            DB::raw("DATE_FORMAT(created_at, '%m-%Y') date"),
            DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
            ->groupBy('year','month')
            ->get();

        return response()->json([
            'data'=>[
                'categories' => ['count'=>$collections,'icon'=>'laptop-house'],
                'devices' => ['count'=>$devices,'icon'=>'desktop'],
                'services' => ['count'=>$services,'icon'=>'laptop-medical'],
                'orders' => ['count'=>$orders,'icon'=>'hand-holding-usd'],
                'banners' => ['count'=>$banner,'icon'=>'sliders-h'],
            ],
            'count_mounts' => [
                'categories' => $collections_count_mounts,
                'devices' => $devices_count_mounts,
                'services' => $services_count_mounts,
                'orders' => $orders_count_mounts,
                'banners' => $banners_count_mounts,
            ]
        ], Response::HTTP_OK);

    }

    public function addDinamicPage(Request $request){
        $rules = [
            'name' =>  'required|unique:pages',
            'title' =>  'required',
            'page_content' =>  'required',
        ];

        $validate = Validator::make($request->all() , $rules);

        if ($validate->fails()){
            return \response()->json(['message' => 'Խնդրում ենք ստուգել բոլեր տվյալները'] , Response::HTTP_BAD_REQUEST);
        }

        Page::create([
            "name" => $request->name,
            "title" => $request->title,
            "content" => $request->page_content,
        ]);

        return \response()->json(['message' => 'Էջը հաջողությամբ ստեղծված է!'] , Response::HTTP_OK);
    }

    public function removeDinamicPage(Request $request){
        Page::find($request->id)->delete();

        return \response()->json(['message' => 'Էջը հաջողությոամբ ջնջված է!'] , Response::HTTP_OK);
    }

    public function editDinamicPage(Request $request){
        $rules = [
            'name' =>  'required|unique:pages',
            'title' =>  'required',
            'page_content' =>  'required',
        ];

        $validate = Validator::make($request->all() , $rules);

        if ($validate->fails()){
            return \response()->json(['message' => 'Խնդրում ենք ստուգել բոլեր տվյալները'] , Response::HTTP_BAD_REQUEST);
        }

        Page::find($request->id)->update($request->request);

        return \response()->json(['message' => 'Էջը հաջողությամբ փոփոխված է!'] , Response::HTTP_OK);
    }

    public function showDinamicPage(Request $request){
        $page = Page::where('name' , $request->name)->first();

        return \response()->json(['page' => $page] , Response::HTTP_OK);
    }

    public function showDynamicList(Request $request){
        $page = Page::all();

        return \response()->json(['menuList' => $page] , Response::HTTP_OK);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllDinamicPage(){
        $pages = Page::all();

        return response()->json([
            'pages' => $pages
        ] , Response::HTTP_OK);
    }

    public function saveSocials(Request $request){
        file_put_contents(base_path('resources/js/data/social.json') , json_encode($request->data));
    }

}
