<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Device extends Model
{
    protected $fillable = [
        'brand',
        'model',
        'collection_id',
        'in_stock',
        'new_price',
        'old_price',
        'made',
        'used',
        'description_am',
        'description_ru',
        'description_en',
        'deposit_details',
        'images'
    ];

    public function parameters(){
        return $this->hasMany(Parameter::class);
    }

    protected static function discount($column = 'old_price', $sort = 'desc'){
        return Device::orderBy($column , $sort)->where('old_price', '>',  0)->where('in_stock' , '>' , 0);
    }

    protected static function news($column = 'created_at', $sort = 'desc'){
        return Device::orderBy($column , $sort)->where('in_stock' , '>' , 0);
    }

    protected static function featured($column = 'new_price', $sort = 'asc'){
        return Device::orderBy($column , $sort)->where('in_stock' , '>' , 0)->select('devices.*', 'collections.path', 'collections.name_en', 'collections.name_ru', 'collections.name_am')->leftJoin('collections' , 'devices.collection_id', '=' ,'collections.id');
    }

    protected static function devices_count_mounts(){
        return Device::select(
            'created_at',
            DB::raw('count(*) as count'),
            DB::raw("DATE_FORMAT(created_at, '%m-%Y') date"),
            DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
            ->groupBy('year','month');
    }

    protected static function withCategories(){
        return Device::select('devices.*',  'collections.path')->leftJoin('collections' , 'devices.collection_id', '=' ,'collections.id');
    }

    protected static function detailed($id){
            $data = Device::where('devices.id', '=', $id)
            ->select('devices.*', 'collections.path')
            ->leftJoin('collections', 'devices.collection_id', '=', 'collections.id')
            ->first();

            $data['parameters'] = Device::find($id)->parameters;

            return $data;
    }


}
