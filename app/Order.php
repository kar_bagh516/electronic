<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        "name",
        "messengers",
        "device_id",
        "surname",
        "address",
        "mobile",
        "email",
        "tatus",
        "pay_method"
    ];
}
