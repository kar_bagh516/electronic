<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    protected $fillable = [
        'device_id',
        'parameter_en',
        'parameter_ru',
        'parameter_am',
        'value',
    ];
}
