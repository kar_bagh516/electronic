<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $fillable = ['name_am' , 'name_ru' , 'name_en' , 'image'];
}
