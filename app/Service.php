<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['name' , 'description_am' ,  'description_ru' ,  'description_en' , 'image'];
}
